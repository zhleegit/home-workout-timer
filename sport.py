import time
from playsound import playsound
import math
from copy import deepcopy
import json
import threading
import os



music ='./alert.wav'
menus = [f for f in os.listdir('menu') if f.startswith('menu') and f.endswith('.py')]

print('======= menu ==============')
for i,m in enumerate(menus):
    print(f'{i}: {m}')
idx = input('choose your menu: ')
idx = int(idx)
assert 0<= idx < len(menus), 'invalid index'
menu_name = menus[idx].split('.')[0]
exec(f'from menu import menu_HIT_1 as menu')
sport_list = menu.sport_list

sport_list.pop() # pop the last rest
sport_list.append([-1 , 'Great! you done!'])
sport_list_acc = [deepcopy(sport_list[0])]


def playsound_job():
    playsound(music)

def show_on_time(current , on , text, alter):
    if current==on:
        print(text)
        threading.Thread(target = playsound_job ).start()
        return True
    else:
        print(alter)
        return False

print("==== raw pairs ====")
for line in sport_list:
    print(line)
s = sport_list[0][0]
for i in range(1,len(sport_list)):
    t,item = sport_list[i]
    s += t
    sport_list_acc.append([s,item])
sport_list = sport_list_acc
print("==== accumulated pairs ====")
for line in sport_list:
    print(line)

input('Ready ? ')
time_count=0
sport_list.append(None)
line = sport_list.pop(0)
prev_on = 0
from_prev = 0
while True:
    on, text_raw = line
    text =  f'::: total: {str(time_count)} :::: Start {text_raw} ::::::::::::::'
    alter = f'::: total: {str(time_count)} :::: from prev: {str(from_prev)}'

    ### calculate time range to approach 1 sec
    start_time = time.time()


    # used for time analysis
    #print(time.time() , end = '  ')
    if show_on_time(time_count , prev_on , text, alter):
        prev_on=on
        from_prev = 0
        line = sport_list.pop(0)
        if line is None:
            break


    ### calculate time rante to approach 1 sec
    while time.time()-start_time < 1:
        continue
    #time.sleep(1)
    time_count +=1
    from_prev +=1



