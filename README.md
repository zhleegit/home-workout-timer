# Home workout timer


## Environment

### OS
- MacOS (havn't been tested on M1 )
### packagees
- playsound
- PyObjC

## usage

### select menu and run it
`python sport.py`

### add your own menu
We use `python list` to configure each menu.
You just create a python named as `menu/menu_*.py`
> you can check `menu/menu_HIT_1.py` to find a template




## time approaching

We use **range difference between start and end** and **fork thread for play sound** to approach each time tick based sec.


### use **sleep based** method
There are lots of bias between each second of time tick
![](./imgs/compute_sec_using_sleep.png)


### use **range difference** method
There may be some delay when playsound event occurs
![](./imgs/compute_sec_using_range.png)

### Hybrid method
The method almost equals to each second of time tick
![](./imgs/compute_sec_using_range_and_fork_playsound.png)


